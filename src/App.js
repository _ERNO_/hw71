import './App.css';
import Random from "./numbers/Random";
import React from "react";


function randomInteger(min, max) {
    let rand = min + Math.random() * (max - min);
    return Math.round(rand);
}



class App extends React.Component {

    state = {
        numbers: [
            {number: randomInteger(1, 5)},
            {number: randomInteger(6, 11)},
            {number: randomInteger(12, 16)},
            {number: randomInteger(17, 23)},
            {number: randomInteger(24, 32)}
        ]
    };

    changeNumber = () => {
        this.setState({
            numbers: [
                {number: randomInteger(1, 5)},
                {number: randomInteger(6, 11)},
                {number: randomInteger(12, 16)},
                {number: randomInteger(17, 23)},
                {number: randomInteger(24, 32)}
            ]
        });
    };

    render() {
        return (
            <>
                <div className="App">
                    <div>
                        <button onClick={this.changeNumber} id="random">
                            New numbers
                        </button>
                    </div>
                    <Random number={this.state.numbers[0].number}/>
                    <Random number={this.state.numbers[1].number}/>
                    <Random number={this.state.numbers[2].number}/>
                    <Random number={this.state.numbers[3].number}/>
                    <Random number={this.state.numbers[4].number}/>
                </div>
            </>
        );
    }
}

export default App;

import React from "react";
import '../App.css';

const Random = props => {
    return (
      <div className="random">
          <div className="circle">
              <p>{props.number}</p>
          </div>
      </div>
    );
}

export default Random;